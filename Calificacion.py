#!/usr/bin/envpython3
# -- coding: utf-8 --


# Se crea la clase que obtendra los datos de calificacion de la pelicula
class Calificacion():
    # Se genera el constructor de la clase junto con los atributos
    def _init_(self):
        self.__calificacion = None

    # Se crea el get de calificacion donde se obtendra
    def get_calificacion(self):
        return calificacion

    # Se crea el set de calificacion que permitira modificarlo
    def set_calificacion(self, calificacion):
        if isinstance(calificacion, int):
            self.__calificacion = calificacion