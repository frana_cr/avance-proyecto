#!/usr/bin/env python3
# -- coding: utf-8 --


# Se crea la clase que obtendra los datos de calificacion de la pelicula
class Pelicula():
    # Se genera el constructor de la clase junto con los atributos de esta
    def _init_(self, pelicula, idioma, genero, pais):
        self.__pelicula = None
        self.__idioma = None
        self.__genero = None
        self.__pais = None

    # Se crea el get del nombre de la pelicula donde se obtendra
    def get_pelicula(self):
        return nombre_pelicula

    # Se crea el set del nombre de la pelicula que permitira modificarlo
    def set_pelicula(self, pelicula):
        if isinstance(pelicula, str):
            self.__pelicula = pelicula

    # Se crea el get de idioma donde se obtendra
    def get_idioma(self):
        return idioma

    # Se crea el set de idioma que permitira modificarlo
    def set_idioma(self, idioma):
        if isinstance(idioma, str):
            self.__idioma = idioma

    # Se crea el get de genero donde se obtendra
    def get_genero(self):
        return genero

    # Se crea el set de genero que permitira modificarlo
    def set_genero(self, genero):
        if isinstance(genero, str):
            self.__genero = genero

    # Se crea el get de pais donde se obtendra
    def get_pais(self):
        return pais

    # Se crea el set de idioma que permitira modificarlo
    def set_pais(self, pais):
        if isinstance(pais, str):
            self.__pais = pais