#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# Se crea la clase que obtendra los datos de la produccion pelicula
class Produccion():
    # Se genera el constructor de la clase junto con los atributos de esta
    def __init__(self):
        self.__escritor = None
        self.__productor = None
        self.__director = None

    # Se crea el get de escritor donde se obtendra
    def get_escritor(self):
        return self.__escritor

    # Se crea el set de escritor que lo modificara
    def set_escritor(self, escritor):
        if isinstance(escritor, str):
            self.__escritor = escritor

    # Se crea el get de productora donde se obtendra
    def get_productor(self):
        return self.__productor

    # Se crea el set productora donde se modificara
    def set_productor(self, productor):
        if isinstance(productor, str):
            self.__productor = productor

    # Se crea el get de nombre director donde se obtendra
    def get_director(self):
        return self.__director

    # Se crea el set de director donde se modificara
    def set_director(self, director):
        if isinstance(director, str):
            self.__director = director