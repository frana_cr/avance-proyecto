#!/usr/bin/env python3
# -- coding: utf-8 --


# se importan clases y libreria
import pandas as pd
from Produccion_Pelicula import Produccion
from Informacion_Pelicula import Pelicula
from Calificacion import Calificacion


# funcion: crea data frame
def filtrar_data():
    # Leer archivo original
    data = pd.read_csv("IMDb movies.csv")

    # Filtra las columnas a desarrollar
    new_data = data.filter(["original_title", "genre", "country", "language", "director", "production_company", "writer", "avg_vote"])

    # Reducir el tamaño del dataset
    new_separacion = new_data[0:102]
    new_separacion.to_csv("new_archivo.csv", sep=";", index=False)


# funcion: crea nuevo data frame a partir del creado anteriormente
def procesa_archivo():
    data = pd.read_csv("new_archivo.csv", sep=";")
    return data


# funcion: genera lista con caracteres no repitidos de listas creadas
# tipo str
def no_repetidos(lista):
    # Se crea la lista que almacenara los datos
    lista_norepetidos = []
    for i in lista:
        # agrega elementos a la lista
        if i not in lista_norepetidos:
            lista_norepetidos.append(i.capitalize())
    print(lista_norepetidos)
    return lista_norepetidos


# funcion: genera lista con caracteres no repetidos de listas creadas
# tipo int
def no_repetidos1(lista):
    # Se crea la lista que almacenara los datos
    lista_norepetidos1 = []
    for i in lista:
        if i not in lista_norepetidos1:
            lista_norepetidos1.append(i)
    print(lista_norepetidos1)
    return lista_norepetidos1


# funcion: crea atributo director de objeto PRODUCCION
def procesa_director(data):
    # Se crea las condiciones
    # Se crea la lista que almacenara los datos
    list_direct = []
    director_encontrado = False
    for director in data:
        blanco = isinstance(director, float)
        # Fila en blanco pasa a la siguiente.
        if(blanco == True):
            pass
        # Si no es blanca continua
        else:
            # Agrega el primer director a la lista
            if not list_direct:
                objeto_director = Produccion()
                objeto_director.set_director(director.capitalize())
                list_direct.append(director.capitalize())
            # De modo contrario se observa si esta en la lista y son agregados los datos
            else:
            # verifica si se encuentra datos repetidos
                for i in list_direct:
                    pass
                    ''' si no es encontrado seran agregados los demas generos
                    De este modo con la lista no repetidos se busca evitar
                    repeticiones de generos. En este caso se repiten pero es
                    debido a que el archivo dentro de un espacio
                    determinado presenta mas de un datos, identificandose el
                    primero con mayus y el segundo comienza con minuscula'''
                if not director_encontrado:
                    objeto_director = Produccion()
                    objeto_director.set_director(director.capitalize())
                    list_direct.append(director.capitalize())
    # se llama a funcion no repetidos, evita caracteres reiterados
    no_repetidos(list_direct)


# funcion: crea atributo productora de objeto PRODUCCION
def procesa_productora(data):
    # Se crea las condiciones
    list_productora = []
    productora_encontrada = False
    for productor in data:
        blanco = isinstance(productor, float)
        # Fila en blanco pasa a la siguiente.
        if(blanco == True):
            pass
        # Si no es blanca continua
        else:
            # Agrega el primer director a la lista
            if not list_productora:
                obj_productor = Produccion()
                obj_productor.set_productor(productor.capitalize())
                list_productora.append(productor.capitalize())
            else:
                # verifica dato que este repetido
                for i in list_productora:
                    pass
                if not productora_encontrada:
                    obj_productor = Produccion()
                    obj_productor.set_productor(productor.capitalize())
                    list_productora.append(productor.capitalize())
    # se llama a funcion, no agrega datos repetidos
    no_repetidos(list_productora)


# funcion: crea atributo de obejto PRODUCCION
def procesa_escritor(data):
    # Se crea las condiciones
    list_escritores = []
    escritor_encontrado = False
    for escritor in data:
        blanco = isinstance(escritor, float)
        # Fila en blanco pasa a la siguiente.
        if(blanco == True):
            pass
        # Si no es blanca continua
        else:
            # Agrega el primer director a la lista
            if not list_escritores:
                objeto_escritor = Produccion()
                objeto_escritor.set_escritor(escritor.capitalize())
                list_escritores.append(escritor.capitalize())
            else:
                # verifica dato que este repetido
                for i in list_escritores:
                    pass
                    ''' si no es encontrado seran agregados los demas generos
                    De este modo con la lista no repetidos se busca evitar
                    repeticiones de generos. En este caso se repiten pero es
                    debido a que el archivo dentro de un espacio
                    determinado presenta mas de un datos, identificandose el primero con mayus y el
                    segundo comienza con minuscula'''
                if not escritor_encontrado:
                    objeto_escritor = Produccion()
                    objeto_escritor.set_escritor(escritor.capitalize())
                    list_escritores.append(escritor.capitalize())
    # se llama a funcion, no agrega datos repetidos
    no_repetidos(list_escritores)


# funcion: crea atributo titulo de pelicula del objeto INFO PELICULA
def procesa_nombre_pelicula(data):
    # Se crea las condiciones
    list_nombre = []
    nombre_encontrado = False
    for pelicula in data:
        blanco = isinstance(pelicula, float)
        # Fila en blanco pasa a la siguiente.
        if(blanco == True):
            pass
        # Si no es blanca continua
        else:
            # Agrega el primer director a la lista y lo capitaliza
            if not list_nombre:
                objeto_pelicula = Pelicula()
                objeto_pelicula.set_pelicula(pelicula.capitalize())
                list_nombre.append(pelicula.capitalize())
            else:
                # verifica dato que este repetido
                for i in list_nombre:
                    pass
                if not nombre_encontrado:
                    objeto_pelicula = Pelicula()
                    objeto_pelicula.set_pelicula(pelicula.capitalize())
                    list_nombre.append(pelicula.capitalize())
    # se llama a funcion, no agrega datos repetidos
    no_repetidos(list_nombre)

# Se crea la funcion que procesa datos del genero
def procesar_genero(data):
    # Se crea la lista donde seran almacenados
    list_generos = []
    # Se definen las condiciones principales
    genero_encontrado = False
    for genero in data:
        blanco = isinstance(genero, float)
        # Fila en blanco pasa a la siguiente.
        if(blanco == True):
            pass
        # Si no es blanca continua
        else:
            # Agrega el primer director a la lista y lo capitaliza
            if not list_generos:
                objeto_genero = Pelicula()
                objeto_genero.set_genero(genero.capitalize())
                list_generos.append(genero.capitalize())
                # Si lo agrega recorrera la lista con generos
            else:
                # De este modo agregara si no esta presente
                for i in list_generos:
                    pass
                    ''' si no es encontrado seran agregados los demas generos
                    De este modo con la lista no repetidos se busca evitar
                    repeticiones de generos. En este caso se repiten pero es
                    debido a que el archivo dentro de un espacio
                    determinado presenta mas de un datos, identificandose
                    el primero con mayus y el segundo comienza con minuscula'''
                if not genero_encontrado:
                    objeto_genero = Pelicula()
                    objeto_genero.set_genero(genero.capitalize())
                    list_generos.append(genero.capitalize())
    no_repetidos(list_generos)


# Se crea la funcion que procesa datos del idioma
def procesa_idioma(data):
    # Se genera la lista que alamacenara datos agregados
    list_idioma = []
    # Se establecen las condiciones
    idioma_encontrado = False
    # Si el idioma esta en data se genera el blanco cuando hay espacios en el DataSet
    for idioma in data:
        blanco = isinstance(idioma, float)
        # Fila en blanco pasa a la siguiente.
        if(blanco == True):
            pass
        # Si no es blanca continua
        else:
            # Agrega el primer director a la lista
            if not list_idioma:
                objeto_idioma = Pelicula()
                objeto_idioma.set_idioma(idioma.capitalize())
                list_idioma.append(idioma.capitalize())
            # De modo contrario se recorre la lista y se agrega el idioma
            else:
                for i in list_idioma:
                    pass
                # En esta forma se agregan los demas idiomas
                if not idioma_encontrado:
                    objeto_idioma = Pelicula()
                    objeto_idioma.set_idioma(idioma.capitalize())
                    list_idioma.append(idioma.capitalize())
    # Con esta funcion se evitan repeticiones de datos
    no_repetidos(list_idioma)


# Se crea la funcion encargada de procesar los paises
def procesar_pais(data):
    # Se crea la lista que almacenara los datos
    list_paises = []
    # Se establecen condiciones
    pais_encontrado = False

    for pais in data:
        blanco = isinstance(pais, float)
        # Fila en blanco pasa a la siguiente.
        if(blanco == True):
            pass
        # Si no es blanca continua
        else:
            # Agrega el primer director a la lista capitaliza
            if not list_paises:
                objeto_pais = Pelicula()
                objeto_pais.set_pais(pais.capitalize())
                list_paises.append(pais.capitalize())
            # De modo contrario se recorre sucede el mismo proceso
            else:
                for i in list_paises:
                    pass
                # Si el pais no es encontrado sera agregado de esta forma
                if not pais_encontrado:
                    objeto_pais = Pelicula()
                    objeto_pais.set_pais(pais.capitalize())
                    list_paises.append(pais.capitalize())
    # Con esta funcion se evitan repeticiones de datos
    no_repetidos(list_paises)

# Se crea la funcion que procesa las calificaciones IMDb
def procesa_calificacion(data):
    # Se crea la lista que almacenara los datos
    list_calificacion = []
    # Se crea las condiciones
    calificacion_encontrado = False
    # En este caso no hay bandera debido a que es un numero no string
    # Si la calificacion esta en el dato la primera es agregada no se puede capitalizar el float
    for calificacion in data:
        if not list_calificacion:
            objeto_calificacion = Calificacion()
            objeto_calificacion.set_calificacion(calificacion)
            list_calificacion.append(calificacion)
        # De modo contrario se observa si esta en la lista y son agregados los datos
        else:
            for i in list_calificacion:
                pass
            # Si no estan, estos son agregados de este modo
            if not calificacion_encontrado:
                objeto_calificacion = Calificacion()
                objeto_calificacion.set_calificacion(calificacion)
                list_calificacion.append(calificacion)
    # Con esta funcion se evitan repeticiones de datos
    no_repetidos1(list_calificacion)

# funcion: Interaccion y recopilacion de datos
def procedimiento_peliculas(data):
    # Interaccion con el usuario
    print("BIENVENIDO AL MOSTRADOR DE PELICULAS:")
    print("visualizar datos de PRODUCCION DE PELICULAS opcion:1")
    print("visualizar datos de INFORMACION DE PELICULAS opcion:2")
    print("visualizar datos sobre CALIFICACION PROMEDIO DE PELICULAS opcion:3")
    # Se pide que ingrese la opcion
    opcion = int(input("INGRESE LA OPCION CORRESPONDIENTE: "))
    opcion2 = True
    # Se genera un while para que se efectue un ciclo para observar los datos
    while opcion2 is True:
        # cuando la opcion es 1 se muestran los datos de la produccion de pelicula
        if (opcion == 1):
            print("MOSTRANDO DATOS SOBRE LA PRODUCCION DE PELICULAS:")
            # Se muestran datos del director
            director = data["director"]
            print("DIRECTORES:")
            procesa_director(director)
            # Se muestran datos de la compañia de produccion
            productora = data["production_company"]
            print("COMPAÑIAS:")
            procesa_productora(productora)
            # Se muestran los datos del escritor
            escritor = data["writer"]
            print("ESCRTORES:")
            procesa_escritor(escritor)
        # Cuando el usuario ingresa la segunda opcion se observa informacion de la pelicula
        elif (opcion == 2):
            print("MOSTRANDO DATOS SOBRE LA INFORMACION DE PELICULAS:")
            # Se muestran los datos del genero
            pelicula = data["original_title"]
            print("PELICULAS:")
            print("\n")
            procesa_nombre_pelicula(pelicula)
            # Se muestran los datos del titulo
            genero1 = data["genre"]
            print("GENEROS:")
            print("\n")
            procesar_genero(genero1)
            # Se muestran los datos del lenguaje
            idioma = data["language"]
            print("IDIOMAS:")
            procesa_idioma(idioma)
            # Se muestran datos referentes al pais de origen
            pais = data["country"]
            print("PAIS ORIGEN PELICULA:")
            procesar_pais(pais)
        # Cuando el usuario ingresa 3 se muestran datos de la calificacion
        elif (opcion == 3):
            print("MOSTRANDO INFORMACION DE LA CALIFICACION:")
            calificacion = data["avg_vote"]
            print("VOTOS DE LOS USUARIOS EN PROMEDIO DE PELICULAS SON:")
            procesa_calificacion(calificacion)
            # Else se cierra el ciclo del While
        else:
            pass
        # Ademas de este modo se termina el programa
        opcion = int(input("SALIR: PRESIONE 0, PARA CONTINUAR: OPCION 1,2 o 3:"))
        if(opcion == 0):
           opcion2 = False


# Se genera el Main donde se filtran, implementan, usan e imprimen los datos
if __name__ == '__main__':
    filtrar_data()
    data = procesa_archivo()
    procedimiento_peliculas(data)